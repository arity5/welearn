# README #

## Synopsis:##
Our proposed project is to create a “smart” crowd-sourced flashcard web application designed particularly for foreign language learning. Each time a user answers a flashcard, they will be asked how easy/hard the question was; this will allow the difficulty weight of each flashcard to be adjusted over time in an iterative process. Complimentarily, the user’s skill level will be adjusted after each flashcard as well. The purpose of this “smart” flashcard system is to match users with questions that are most closely similar to their designated level. This is unlike any other flashcard system that we know of, and only works in our case because the entire application is crowd-sourced with the assumption there is a large number of users.

Since anyone can add new questions in the categories (e.g. “French”, “Spanish”, “Mandarin Chinese”), we propose that moderation generally be crowd-sourced, although there will also be a moderator role as well. Every flashcard will have a “report” button as well as an optional rating on the “quality” of the question design. Flashcards with poor quality ratings will be shown less frequently over time. When flashcards have been reported too many times (e.g. user feedback says the answer is flat out wrong), it will be removed.

For extra bells-and-whistles (that may beyond our baseline requirements), we propose:

* Users should be able to create flashcards of multiple types.
e.g. multiple-choice, reading comprehension, listening comprehension.

* Users will be able to save public flashcards they like into their own collections.

* Users will be able to browse all public flashcards in a reasonable fashion

* Search/category/tagging/metadata system for all questions.

* A capacity to select many multiple questions at once

* They can later practice/review mastering these collections with spaced-repetition algorithms.

* Users should be able to import/export large collections of flashcards.

* Users should be able to monitor their progress over time.

* Users should be able to view the statistics on each flashcard.

* We need to have a mechanism to handle questions that may have multiple correct answers. For instance, there may be multiple ways to say: “hello/hi”

* Moderators should be able to review a list of recently added flashcards and mark them as “patrolled” so a team of moderators do not duplicate work.

* We should be able to define difficulty-level benchmarks so users can stay informed while manually adjusting their difficulty level.

## User Stories Version 0.1: ##

**Register/Login/Logout:** 
As a user, I would like to register for the web application so it can save my data. From here, I can log in and out of my account.

**Adjust User Skill Level:**
As a user, I would like to adjust my starting difficulty level for a particular category. I would also like to adjust other settings (e.g. weights) related the the automatic adjustment of my skill level and the flashcards I am dispensed.

**Add a New Flashcard:**
As a user, I would like to add a new flashcard to the public repository under a particular category.

**Manage My Flashcards:**
As a user, I would like to be able to find all the flashcards I personally added in one place. I would like to be able to edit/delete them if necessary.

**Dispense a Public Flashcard:**
As a user, I would like to begin drawing flashcards from the deck. I would like to be able to flip the flashcard and quickly move to the next one.

**Rate a Public Flashcard:**
As a user, I would like to rate each flashcard as easy/medium/hard. I would also like to rate each flashcard as good/fair/poorly designed.