package models;

import models.*;
import org.junit.*;
import static org.junit.Assert.*;
import play.test.WithApplication;
import static play.test.Helpers.*;

/**
 * Tests the User Model
 */
public class UserTest extends WithApplication {
    
    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase()));
    }

    /**
     * Test creating and saving a user. Then see if they are in the database.
     */
    @Test
    public void createAndRetrieveUser() {
	    new User("bob", "bob@gmail.com", "secret").save();
	    User bob = User.find.where().eq("username", "bob").findUnique();
	    assertNotNull(bob);
	    assertEquals("bob@gmail.com", bob.email);
    }

    /**
     * Test that creating a username and email is case-insensitive
     */
    @Test
    public void createAndRetrieveInsenstiveUser() {
        new User("BOB", "BoB@GMAiL.coM", "secret").save();
        User bob = User.find.where().eq("username", "bob").findUnique();
        assertNotNull(bob);
        assertEquals("bob@gmail.com", bob.email);
    }

    /**
     * Test the authentication method in the user model.
     */
    @Test
    public void tryAuthenticateUser() {
	    new User("Bob", "bob@gmail.com", "secret").save();
	    assertNotNull(User.authenticate("bob", "secret"));
	    assertNull(User.authenticate("bob", "badpassword"));
	    assertNull(User.authenticate("tom", "secret"));
    }

    /**
     * Test that setSkill and getSkill method works
     */
    @Test
    public void testGetSetSkill() {
        // Check initial value of user skill
        User bob = new User("bob", "bob@gmail.com", "secret");
        bob.save();
        assertEquals(1.0, bob.getSkill(), 0.001d);

        // Check changing this initial value
        bob.setSkill(3.0);
        assertEquals(3.0, bob.getSkill(), 0.001d);

        // Check adding a flashcard rating and then reseting the skill value again
        new Flashcard("title", "How are you 1?", "Fine1.", "bob", false).save();
        new Rating(Rating.DIFFICULTY, "bob", 1, 99999).save();
        bob.setSkill(4.0);
        assertEquals(4.0, bob.getSkill(), 0.001d);
    }
    //Test getPassword()
    @Test
    public void testGetPassword(){
        User bob = new User("bob", "bob@gmail.com", "secret");
        bob.save();
        String p = bob.getPassword();
        assertEquals(p,"******");
    }


}







