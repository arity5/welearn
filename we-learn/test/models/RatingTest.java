package models;

import org.junit.*;
import play.test.WithApplication;
import java.util.*;
import models.*;
import scala.concurrent.java8.FuturesConvertersImpl;

import static org.junit.Assert.*;
import static play.test.Helpers.*;

/**
 * Tests the Rating Model
 */
public class RatingTest extends WithApplication {

    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase()));
        User bob = new User("bob", "bob@gmail.com", "secret");
        User jane = new User("jane", "jane@gmail.com", "secret");
        bob.save();
        jane.save();
        new Flashcard("title", "How are you 1?", "Fine1.", "bob", false).save();
        new Flashcard("title2", "How are you 2?", "Fine2.", "jane", false).save();
        new Flashcard("title3", "How are you 3?", "Fine3.", "bob", false).save();
        new Flashcard("titl4", "How are you 4?", "Fine4.", "jane", false).save();
        new Flashcard("title5", "How are you 5?", "Fine5.", "bob", false).save();
    }

    /**
     * Test creating and saving a rating. Then see if they are in the database.
     */
    @Test
    public void createRatingTest() {
        new Rating(Rating.DIFFICULTY, "bob", 1, 3).save();
        Rating test = Rating.find.where().eq("id", 1).findUnique();
        assertEquals("bob@gmail.com", test.rater.email);
    }

    /**
     * Test creating and saving a rating when User or Flashcard does not exist in DB.
     */
    @Test
    public void createRatingExceptionTest() {
        try {
            new Rating(Rating.DIFFICULTY, "Zeus", 1, 3).save();
        }
        catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Error: Username does not exist in db.");
        }
        try {
            new Rating(Rating.DIFFICULTY, "bob", 999999, 3).save();
        }
        catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Error: Flashcard does not exist in db.");
        }
    }

    /**
     * Test that User.getSkill method works with the proper numbers when new Ratings are added
     */
    @Test
    public void testUserGetSkill() {
        User bob = User.find.byId("bob");
        assertEquals(1, bob.getSkill(), 0.001);
        bob.save();
        new Rating(Rating.DIFFICULTY, "bob", 2, 3).save();
        assertEquals(2, bob.getSkill(), 0.001);
        new Rating(Rating.DIFFICULTY, "bob", 1, 5).save();
        new Rating(Rating.DIFFICULTY, "jane", 1, 99).save();
        new Rating(Rating.QUALITY, "bob", 1, 9999).save();
        assertEquals(3, bob.getSkill(), 0.001);
    }

    /**
     * Test that User.setSkill method resets user skill level even with existing flashcard ratings
     */
    @Test
    public void testUserSetSkillReset() {
        User bob = User.find.byId("bob");
        bob.save();
        new Rating(Rating.DIFFICULTY, "bob", 2, 3).save();
        new Rating(Rating.DIFFICULTY, "bob", 1, 4).save();
        bob.setSkill(5);
        assertEquals(5, bob.getSkill(), 0.001);
        new Rating(Rating.DIFFICULTY, "bob", 1, 3).save();
        assertEquals(4, bob.getSkill(), 0.001);
    }

    /**
     * Test that computeQuality and computeDifficulty method works
     */
    @Test
    public void testFlashcardRatingCompute() {
        /*
        Flashcard fc = Flashcard.find.ref(Long.valueOf(1));
        assertEquals(1, fc.computeDifficulty(), 0.001);
        assertEquals(3, fc.computeQuality(), 0.001);
        new Rating(Rating.DIFFICULTY, "bob", 1, 3).save();
        assertEquals(3, fc.computeDifficulty(), 0.001);
        assertEquals(3, fc.computeQuality(), 0.001);
        new Rating(Rating.DIFFICULTY, "bob", 1, 5).save();
        assertEquals(3, fc.computeDifficulty(), 0.001);
        assertEquals(5, fc.computeQuality(), 0.001);
        new Rating(Rating.DIFFICULTY, "jane", 1, 5).save();
        new Rating(Rating.DIFFICULTY, "bob", 2, 9999).save();
        assertEquals(4, fc.computeDifficulty(), 0.001);
        */
    }

}
