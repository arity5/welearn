package models;


import org.junit.*;
import play.test.WithApplication;
import java.util.*;
import models.*;
import static org.junit.Assert.*;
import static play.test.Helpers.*;

/**
 * Tests the Flashcard Model
 */
public class FlashcardTest extends WithApplication {
    
    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase()));
        new User("Bob", "bob@gmail.com", "secret").save();
        new User("Jane", "jane@gmail.com", "secret").save();
        new User("Doe", "doe@gmail.com", "secret").save();
    }

    /**
     * Test creating and saving a flashcard. Then see if they are in the database.
     */
    @Test
    public void createAndRetrieveFlashcard() {
	    User bob = User.find.where().eq("username", "bob").findUnique();
        new Flashcard("title", "How are you?", "Fine.", bob.username, false).save();
        Flashcard test = Flashcard.find.where().eq("name", "title").findUnique();
	    assertEquals("bob@gmail.com", test.creator.email);
    }

    /**
     * Test the find involving method
     */
    @Test
    public void findProjectsInvolving() {
        new Flashcard("title3", "Hi?", "Bye.", "bob", false).save();
        new Flashcard("title4", "Whee?", "NO.", "bob", true).save();
        new Flashcard("title5", "Run!", "That's not a question!", "jane", false).save();

        List<Flashcard> results = Flashcard.findInvolving("bob");
        assertEquals(2, results.size());
        assertEquals("title3", results.get(0).name);

        results = Flashcard.findInvolving("doe");
        assertEquals(0, results.size());
    }
    
    /**
     * Test search by name method
     */
    @Test
    public void searchByName() {
	    User bob = User.find.where().eq("username", "bob").findUnique();
        new Flashcard("title", "How are you?", "Fine.", bob.username, false).save();
        List<Flashcard> flashcards = Flashcard.searchByTitle("test");
	    assertEquals(flashcards.size(), 0);
	    new Flashcard("test", "How are you?", "Fine.", bob.username, false).save();
	    flashcards = Flashcard.searchByTitle("test");
	    assertEquals(flashcards.size(), 1);
    }

}