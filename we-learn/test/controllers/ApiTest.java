package controllers;

import java.util.*;
import models.*;
import org.junit.*;
import static org.junit.Assert.*;
import play.mvc.*;
import play.mvc.Http.RequestBuilder;
import play.libs.*;
import play.test.*;
import static play.test.Helpers.*;

import com.google.common.collect.ImmutableMap;
import play.twirl.api.Content;
import play.libs.Json;

public class ApiTest extends WithApplication {

    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase(), fakeGlobal()));
        new User("bob", "bob@gmail.com", "secret").save();
        new Flashcard("Title", "How are you?", "I'm doing well.", "bob", false).save();
    }

    @Test
    public void testGetUser() {
        Result result = route(
                controllers.routes.Api.getUser("bob")
                );
        assertEquals(OK, result.status());
        assertEquals("application/json", result.contentType());
        assertTrue(contentAsString(result).contains("bob@gmail.com"));
    }

    @Test
    public void testAddUser() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "jane");
        data.put("email", "jane@email.com");
        data.put("password", "secret");
        final Call addUser = controllers.routes.Api.addUser();
        Result result = route(
                new RequestBuilder()
                        .method(addUser.method())
                        .uri(addUser.url())
                        .bodyJson(Json.toJson(data))
        );
        assertEquals(OK, result.status());
        assertEquals("application/json", result.contentType());
        User jane = User.find.where().eq("username", "jane").findUnique();
        assertNotNull(jane);
        assertEquals("jane@email.com", jane.email);
    }

    @Test
    public void testEditUserUnauthenticated() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "bob");
        data.put("email", "bobnewemail@email.com");
        data.put("password", "secret2");
        final Call editUser = controllers.routes.Api.editUser("bob");
        Result result = route(
                new RequestBuilder()
                        .method(editUser.method())
                        .uri(editUser.url())
                        .bodyJson(Json.toJson(data))
        );
        assertEquals(401, result.status());
        User bob = User.find.where().eq("username", "bob").findUnique();
        assertNotNull(bob);
        assertEquals("bob@gmail.com", bob.email);
    }

    @Test
    public void testEditUserAuthenticated() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "bob");
        data.put("email", "bobnewemail@email.com");
        data.put("password", "secret2");
        final Call editUser = controllers.routes.Api.editUser("bob");
        Result result = route(
                new RequestBuilder()
                        .method(editUser.method())
                        .uri(editUser.url())
                        .session("user", "bob")
                        .bodyJson(Json.toJson(data))
        );
        assertEquals(200, result.status());
        User bob = User.find.where().eq("username", "bob").findUnique();
        assertNotNull(bob);
        assertEquals("bobnewemail@email.com", bob.email);

    }


    /**
     * This test doesn't work right now because we haven't described how cascading delete yet.
     * For instance, if we delete a user, should the flashcards associated with that user be deleted
     * as well?
     */
    /*
    @Test
    public void testDeleteUser() {
        final Call addUser = controllers.routes.Api.deleteUser("Bob");
        Result result = route(
                new RequestBuilder()
                        .method(addUser.method())
                        .uri(addUser.url())
        );
        assertEquals(OK, result.status());
        User bob = User.find.where().eq("username", "Bob").findUnique();
        assertNull(bob);
    }
    */



    @Test
    public void testGetFlashcard() {
        Result result = route(
                controllers.routes.Api.getFlashcard(1)
        );
        assertEquals(OK, result.status());
        assertEquals("application/json", result.contentType());
        assertTrue(contentAsString(result).contains("I'm doing well"));
    }

    @Test
    public void getFlashcardsByUser() {
        final Call getFlashcardsByUser = controllers.routes.Api.getFlashcardsByUser("bob", 10, 0);
        Result result = route(
                new RequestBuilder()
                        .method(getFlashcardsByUser.method())
                        .uri(getFlashcardsByUser.url())
        );
        assertEquals(OK, result.status());
        assertTrue(contentAsString(result).contains("]"));
    }

    @Test
    public void testAddFlashcard() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("name", "This is a title");
        data.put("question", "What is the weather like?");
        data.put("answer", "Bad.");
        data.put("creator", "bob");
        data.put("isDraft", "false");
        final Call addFlashcard = controllers.routes.Api.addFlashcard();
        Result result = route(
                new RequestBuilder()
                        .method(addFlashcard.method())
                        .uri(addFlashcard.url())
                        .session("user", "bob")
                        .bodyJson(Json.toJson(data))
        );
        assertEquals(OK, result.status());
        Flashcard flashcard = Flashcard.find.where().eq("name", "This is a title").findUnique();
        assertNotNull(flashcard);
        assertEquals("What is the weather like?", flashcard.question);
    }


    @Test
    public void testEditFlashcard() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("id", "1");
        data.put("name", "Title");
        data.put("question", "How are you doing?");
        data.put("answer", "I think I am dying!");
        data.put("creator", "bob");
        data.put("isDraft", "false");
        final Call editFlashcard = controllers.routes.Api.editFlashcard(1);
        Result result = route(
                new RequestBuilder()
                        .method(editFlashcard.method())
                        .uri(editFlashcard.url())
                        .session("user", "bob")
                        .bodyJson(Json.toJson(data))
        );
        assertEquals(OK, result.status());
        Flashcard flashcard = Flashcard.find.where().eq("name", "Title").findUnique();
        assertNotNull(flashcard);
        assertEquals("I think I am dying!", flashcard.answer);
    }

    @Test
    public void testSearchFlashcards() {
        Result result = route(
                controllers.routes.Api.getFlashcardsByName("Title")
        );
        assertEquals(OK, result.status());
        assertEquals("application/json", result.contentType());
        assertTrue(contentAsString(result).contains("I'm doing well"));
        result = route(
                controllers.routes.Api.getFlashcardsByName("aaa")
        );
        assertTrue(contentAsString(result).contains("I'm doing well") == false);
    }
    
    /**
     * This test doesn't work right now because we haven't described how cascading delete yet.
     * For instance, if we delete a flashcard, should the ratings associated with that flashcard be deleted
     * as well?
     */
    /*
    @Test
    public void testDeleteFlashcard() {
        final Call deleteFlashcard = controllers.routes.Api.deleteFlashcard(1);
        Result result = route(
                new RequestBuilder()
                        .method(deleteFlashcard.method())
                        .uri(deleteFlashcard.url())
        );
        assertEquals(OK, result.status());
        Flashcard flashcard = Flashcard.find.where().eq("name", "Title").findUnique();
        assertNull(flashcard);
    }
    */

    @Test
    /**
     * Skipping this test for now because I cannot resolve how to create a hash of both string and number values.
     */
    public void testAddRating() {
        /*
        Map<String, String> data = new HashMap<String, String>();
        data.put("type", 0);
        data.put("rater", "bob");
        data.put("flashcard", 1);
        data.put("value", 4.0);

        final Call rateFlashcard = controllers.routes.Api.addRating();
        Result result = route(
                new RequestBuilder()
                        .method(rateFlashcard.method())
                        .uri(rateFlashcard.url())
                        .bodyJson(Json.toJson(data))
        );
        assertEquals(OK, result.status());
        Flashcard flashcard = Flashcard.find.where().eq("name", "Title").findUnique();
        assertNotNull(flashcard);
        assertEquals(4, flashcard.getDifficultyRating(), 0.0001d);
        */
    }

}
