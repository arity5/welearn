package controllers;

import org.junit.*;
import static org.junit.Assert.*;
import models.User;
import play.mvc.Call;
import play.mvc.Http;
import play.mvc.Result;
import play.libs.*;

import static play.test.Helpers.*;

/**
 * This tests the authenticator decorator
 */
public class AuthenticatorTest {

    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase(), fakeGlobal()));
        new User("bob", "bob@gmail.com", "secret").save();
    }

    /**
     * Make sure users who should be allowed to access an automatically authenticated page can access it.
     */
    @Test
    public void authenticated() {
        final Call authenticate = controllers.routes.Application.userHome();
        Result result = route(
                new Http.RequestBuilder().method(authenticate.method()).uri(authenticate.url()).session("user", "bob")
        );
        assertEquals(200, status(result));
    }

    /**
     * Make sure users who should NOT be allowed to access a page can NOT access it.
     */
    @Test
    public void notAuthenticated() {
        final Call authenticate = controllers.routes.Application.userHome();
        Result result = route(
                new Http.RequestBuilder().method(authenticate.method()).uri(authenticate.url())
        );
        assertEquals(303, status(result));
        assertEquals("/user/login", header("Location", result));
    }
}
