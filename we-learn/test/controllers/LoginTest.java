package controllers;


import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

import play.mvc.*;
import play.mvc.Http.RequestBuilder;
import play.libs.*;
import play.test.*;
import static play.test.Helpers.*;
import models.*;

/**
 * Tests login functionality from a controller perspective.
 */
public class LoginTest extends WithApplication {
    
    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase(), fakeGlobal()));
        new User("Bob", "bob@gmail.com", "secret").save();
    }

    /**
     * Test a user with the right credentials can log in.
     */
    @Test
    public void authenticateSuccess() {
        Map<String, String> data = new HashMap<String, String>();
		data.put("username", "Bob");
		data.put("password", "secret");
        final Call authenticate = controllers.routes.Application.login();
        Result result = route(new RequestBuilder().method(authenticate.method()).uri(authenticate.url()).bodyForm(data));
        assertEquals(303, status(result));
        assertEquals("Bob", session(result).get("user"));
    }

    /**
     * Test a user with the wrong credentials cannot log in.
     */
    @Test
    public void authenticateFailure() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "Bob");
        data.put("password", "badpassword");
        final Call authenticate = controllers.routes.Application.login();
        Result result = route(new RequestBuilder().method(authenticate.method()).uri(authenticate.url()).bodyForm(data));

        assertEquals(400, status(result));
        assertNull("Bob", session(result).get("user"));
    }

    @Test
    public void authFailBCMissingField(){
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "");
        data.put("password", "secret");
        final Call authenticate = controllers.routes.Application.login();
        Result result = route(new RequestBuilder().method(authenticate.method()).uri(authenticate.url()).bodyForm(data));
        assertEquals(400, status(result));
        assertNull("Bob", session(result).get("user"));        
    }

}