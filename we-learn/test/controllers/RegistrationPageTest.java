package controllers;

import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

import play.mvc.*;
import play.mvc.Http.RequestBuilder;
import play.libs.*;
import play.test.*;
import static play.test.Helpers.*;
import models.*;

/**
 * Tests registration functionliarty
 */
public class RegistrationPageTest {

    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase(), fakeGlobal()));
    }

    /**
     * Test the sunny-day flow for user registration.
     */
    @Test
    public void registerSuccess() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "Bob");
        data.put("email", "bob@gmail.com");
        data.put("password", "secret");
        final Call register = controllers.routes.Application.register();
        Result result = route(new RequestBuilder().method(register.method()).uri(register.url()).bodyForm(data));
        assertEquals(303, status(result));

        User bob = User.find.where().eq("username", "Bob").findUnique();
        assertNotNull(bob);
        assertEquals("bob@gmail.com", bob.email);
    }

    //Test registering the same user
    @Test
    public void registerSame(){
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "Bob");
        data.put("email", "bob@gmail.com");
        data.put("password", "secret");
        final Call register = controllers.routes.Application.register();
        Result result = route(new RequestBuilder().method(register.method()).uri(register.url()).bodyForm(data));
        assertEquals(303, status(result));

        result = route(new RequestBuilder().method(register.method()).uri(register.url()).bodyForm(data));
        assertEquals(400, status(result));
    }

    //Test registering the same user but different email
    @Test
    public void registerSameWithDifEmail(){
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "Bob");
        data.put("email", "bob2@gmail.com");
        data.put("password", "secret");
        final Call register = controllers.routes.Application.register();
        Result result = route(new RequestBuilder().method(register.method()).uri(register.url()).bodyForm(data));
        assertEquals(303, status(result));

        result = route(new RequestBuilder().method(register.method()).uri(register.url()).bodyForm(data));
        assertEquals(400, status(result));
    }

    //Test with user information missing
    @Test
    public void registerBadInput() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("username", "Bob");
        data.put("email", "");
        data.put("password", "secret");
        final Call register = controllers.routes.Application.register();
        Result result = route(new RequestBuilder().method(register.method()).uri(register.url()).bodyForm(data));
        assertEquals(400, status(result));

        User bob = User.find.where().eq("username", "Bob").findUnique();
        assertNull(bob);
        
        data = new HashMap<String, String>();
        data.put("username", "");
        data.put("email", "bob@gmail.com");
        data.put("password", "secret");
        result = route(new RequestBuilder().method(register.method()).uri(register.url()).bodyForm(data));
        assertEquals(400, status(result));

        bob = User.find.where().eq("username", "Bob").findUnique();
        assertNull(bob);

        data = new HashMap<String, String>();
        data.put("username", "Bob");
        data.put("email", "bob@gmail.com");
        data.put("password", "");
        result = route(new RequestBuilder().method(register.method()).uri(register.url()).bodyForm(data));
        assertEquals(400, status(result));

        bob = User.find.where().eq("username", "Bob").findUnique();
        assertNull(bob);
    }


    /**
     * Need to write failure cases (e.g. when the username or email already exists)
     *
     *
     * More tests need to be written....
     *
     *
     */
}
