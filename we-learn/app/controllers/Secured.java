package controllers;

import play.*;
import play.mvc.*;
import play.mvc.Http.*;
import play.libs.Json;
import com.fasterxml.jackson.databind.*;
import models.*;

/**
 * This is the authenticator class that describes a decorator for actions that need to be authenticated.
 *
 * Refer to the 2.1.1 authentication tutorial to see where this code came from. It is actually used
 * in the Application controller (for a demonstration in action). It may be an option to look into how
 * to extend this for other permissions based things -- alternatively we could just hand-code our own.
 */
public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx) {
        return ctx.session().get("user");
    }

    @Override
    public Result onUnauthorized(Context ctx) {
        JsonNode json = ctx.request().body().asJson();
        if(json == null) {
            // Request is not JSON
            return redirect(routes.Application.loginPage());
        }

        return status(401, Json.toJson("Error: JSON request is unauthenticated."));
    }
}
