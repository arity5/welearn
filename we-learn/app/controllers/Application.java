package controllers;

import play.*;
import play.data.*;
import play.libs.Json;
import play.mvc.*;
import views.forms.LoginForm;
import views.forms.RegistrationForm;
import views.html.*;
import java.util.*;
import static play.libs.Json.toJson;

import play.db.ebean.*;
import models.*;

public class Application extends Controller {

    /**
     * This action returns the homepage.
     *
     * Security authentication was previously on this page for demonstration purposes, but not anymore. Refer to
     * previous commits.
     */
    public Result index() {
        return ok(index.render("Welcome."));
    }

    /**
     * This action returns the registration page (GET).
     */
    public Result registrationPage() {
        return ok(register.render(Form.form(RegistrationForm.class)));
    }

    /**
     * This action attempts to register a user (POST).
     *
     * Requires form data to be part of the request.
     */
    public Result register() {
        Form<RegistrationForm> registrationForm = Form.form(RegistrationForm.class).bindFromRequest();
        if (registrationForm.hasErrors()) {
            return badRequest(register.render(registrationForm));
        } else {
            User newUser = Form.form(User.class).bindFromRequest().get();
            //TODO: setting defaults should not happen here
            newUser.initialSkill = 1;
            newUser.collection = "";
            newUser.save();
            return redirect(routes.Application.loginPage());
        }
    }

    /**
     * This method returns the login page.
     */
    public Result loginPage() { return ok(login.render(Form.form(LoginForm.class))); }

    /**
     * This action attempts to login a user (POST).
     *
     * Requires form data to be part of the request.
     */
    public Result login() {
        Form<LoginForm> loginForm = Form.form(LoginForm.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(login.render(loginForm));
        }
        else {
            session().clear();
            session("user", loginForm.get().username);
            return redirect(routes.Application.userHome());
        }
    }

    /**
     * This action logs out a user (GET).
     */
    public Result logout() {
        session().clear();
        flash("success", "You've been logged out");
        return redirect(
                routes.Application.loginPage()
        );
    }

    /**
     * This action get's a user's personal control panel(GET).
     *
     * Note that the security authenticator is set such that the user must be logged in to access this page.
     */
    @Security.Authenticated(Secured.class)
    public Result userHome() {
        return ok(user.render("User Homepage", User.find.where().eq("username", request().username()).findUnique()));
    }

    @Security.Authenticated(Secured.class)
    public Result settings() {
        return ok(settings.render("User Settings", User.find.where().eq("username", request().username()).findUnique()));
    }
    
    @Security.Authenticated(Secured.class)
    public Result search() {
        return ok(search.render("Search", User.find.where().eq("username", request().username()).findUnique()));
    }

    @Security.Authenticated(Secured.class)
    public Result moderationHome() {
        return ok(moderation.render("Moderation Homepage", User.find.where().eq("username", request().username()).findUnique()));
    }

    /**
     * Get All Users (GET)
     *
     * This is a deprecated method. The intention is to remove it eventually.
     */
    public Result getUser() {
        List<User> users = new Model.Finder(String.class, User.class).all();
        return ok(Json.toJson(users));
    }
    
    /**
     * Play (GET)
     * 
     * Goes to game page
     */
    @Security.Authenticated(Secured.class)
    public Result play() {
        return ok(game.render("Play", User.find.where().eq("username", request().username()).findUnique()));
    }

    /**
     * Javascript Router
     */
    public Result javascriptRoutes() {
        response().setContentType("text/javascript");
        return ok(
                Routes.javascriptRouter("jsRoutes",
                        // Routes
                        controllers.routes.javascript.Api.editUser(),
                        controllers.routes.javascript.Api.getFlashcard(),
                        controllers.routes.javascript.Api.getFlashcardsByUser(),
                        controllers.routes.javascript.Api.editFlashcard(),
                        controllers.routes.javascript.Api.deleteFlashcard(),
                        controllers.routes.javascript.Api.saveFlashcard()
                )
        );
    }

    // Testing
    public Result test() {
        return ok(test.render("Test"));
    }
}
