package controllers;

import play.*;
import play.data.*;
import play.mvc.*;
import java.util.*;
import play.libs.Json;
import com.fasterxml.jackson.databind.*;

import play.db.ebean.*;
import models.*;
import scala.concurrent.java8.FuturesConvertersImpl;

/**
 * Project WeLearn - Flashcard Applicataion
 * COMSW4156 Advanced Software Engineering
 * Api.java
 * Purpose: Dedicated API endpoint for the application
 *
 * @author  Arity5
 * @version 0.2
 */
public class Api extends Controller {

    /**
     * Get User (GET)
     *
     * @param id    the user's username
     * @return      json representation of the user
     */
    public Result getUser(String id) {
        User user = User.find.byId(id);
        if (user == null) {
            return notFound(Json.toJson("Error: Username not found."));
        }
        return ok(Json.toJson(user));
    }

    /**
     * Get All Users (GET)
     *
     * @return      json representation of the users
     */
    public Result getUsers() {
        List<User> users = new Model.Finder(String.class, User.class).all();
        if (users.size() == 0) {
            return notFound(Json.toJson("No users in database."));
        }
        return ok(Json.toJson(users));
    }

    /**
     * Create User (GET)
     *
     * @return      validation errors or success message
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result addUser() {

        // Parse
        JsonNode json = request().body().asJson();
        String username = json.findPath("username").textValue();
        String email = json.findPath("email").textValue();
        String password = json.findPath("password").textValue();

        // Data Validation
        if (username == null ) { return badRequest(Json.toJson("Missing parameter [username]")); }
        if (email == null ) { return badRequest(Json.toJson("Missing parameter [email]")); }
        if (password == null ) { return badRequest(Json.toJson("Missing parameter [password]")); }
        if (User.find.byId(username) != null) {
            return status(409, Json.toJson("Username taken."));
        }
        if (!email.contains("@") || !email.contains(".")) {
            return badRequest(Json.toJson("This is an invalid email address."));
        }

        // Create User
        new User(username, email, password).save();
        return ok(Json.toJson("Success: 1 User created."));

    }

    /**
     * Edit User (PUT)
     *
     * @return      validation errors or success message
     */
    @BodyParser.Of(BodyParser.Json.class)
    @Security.Authenticated(Secured.class)
    public Result editUser(String id) {

        // Parse
        JsonNode json = request().body().asJson();
        JsonNode username = json.findPath("username");
        JsonNode email = json.findPath("email");
        JsonNode password = json.findPath("password");
        JsonNode skill = json.findPath("skill");

        // Data Validation and Deserialization
        boolean dataChanged = false;
        User user = User.find.byId(id.toLowerCase());
        if (user == null) {
            return notFound(Json.toJson("Error: Username not found."));
        }
        if (!username.isNull()) {
            if (!username.textValue().equals(id)) {
                return badRequest(Json.toJson("Contradictory User id's provided"));
            }
        }
        if (!email.isNull()) {
            if (!email.textValue().contains("@") || !email.textValue().contains(".")) {
                return badRequest(Json.toJson("This is an invalid email address."));
            }
            user.email = email.textValue();
            dataChanged = true;
        }
        if (!password.isNull()) { user.password = password.textValue(); dataChanged = true; }
        if (!skill.isNull()) {
            user.setSkill(skill.doubleValue());
            dataChanged = true;
        }
        if(!dataChanged) {
            return ok(Json.toJson("Success: No data changed."));
        }

        // Update User
        user.update();
        return ok(Json.toJson("Success: 1 User updated."));

    }

    /**
     * Delete User (DELETE)
     * TODO: Need to determine whether associated flashcards and ratings should cascading delete
     *
     * @return      validation errors or success message
     */
    @Security.Authenticated(Secured.class)
    public Result deleteUser(String id) {
        User user = User.find.byId(id);
        if (user == null) {
            return notFound(Json.toJson("Error: Username not found."));
        }
        User.find.ref(user.username).delete();
        return ok(Json.toJson("Success: 1 User deleted."));
    }

    /**
     * Get Flashcard (GET)
     *
     * @param id    the flashcard's id
     * @return      json representation of the flashcard
     */
    public Result getFlashcard(Long id) {
        Flashcard flashcard = Flashcard.find.byId(id);
        if (flashcard == null) {
            return notFound(Json.toJson("Flashcard id not found."));
        }
        return ok(Json.toJson(flashcard));
    }

    /**
     * Get All Flashcards (GET)
     *
     * @return      json representation of the flashcards
     */
    public Result getFlashcards() {
        List<Flashcard> flashcards = new Model.Finder(String.class, Flashcard.class).all();
        if (flashcards.size() == 0) {
            return notFound(Json.toJson("No flashcards in database."));
        }
        return ok(Json.toJson(flashcards));
    }
    
    /**
     * Get All Flashcards similar to Name(GET)
     * @param name  the flashcard's name
     *
     * @return      json representation of the users
     */
    public Result getFlashcardsByName(String name) {
        List<Flashcard> flashcards = Flashcard.searchByTitle(name);
        //List<Flashcard> flashcards = new Model.Finder(String.class, Flashcard.class).all();
        if (flashcards.size() == 0) {
            return notFound(Json.toJson("No flashcards in database."));
        }
        return ok(Json.toJson(flashcards));
    }

    /**
     * Get All Flashcards that belong to a certain user (GET)
     *
     * @param user      username of the User whom the flashcards belong to
     * @param limit     maximum number of flashcards to return
     * @param offset    number of flashcards to skip
     * @return          json represenation of the flashcards
     */
    public Result getFlashcardsByUser(String user, int limit, int offset) {
        List<Flashcard> flashcards = Flashcard.findInvolving(user);
        if(offset+limit > flashcards.size()) {
            flashcards = flashcards.subList(offset, flashcards.size());
        }
        else {
            flashcards = flashcards.subList(offset, offset+limit);
        }
        if (flashcards.size() == 0) {
            return notFound(Json.toJson("No flashcards found."));
        }
        return ok(Json.toJson(flashcards));
    }

    /**
     * Get All Reports (GET)
     *
     * @return      json representation of the reports
     */
    public Result getReports() {
        List<Report> reports = new Model.Finder(String.class, Report.class).all();
        if (reports.size() == 0) {
            return notFound(Json.toJson("No reports in database."));
        }
        return ok(Json.toJson(reports));
    }

    /**
     * Get All New Reports (GET)
     *
     * @return      json representation of the reports
     */
    public Result getNewReports() {
        List<Report> reports = Report.find.where().eq("status","new").findList();
        if (reports.size() == 0) {
            return notFound(Json.toJson("No new reports in database."));
        }
        return ok(Json.toJson(reports));
    }

    /**
     * Create Flashcard (POST)
     *
     * @return      validation errors or success message
     */
    @BodyParser.Of(BodyParser.Json.class)
    @Security.Authenticated(Secured.class)
    public Result addFlashcard() {

        // Parse
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").textValue();
        String question = json.findPath("question").textValue();
        String answer = json.findPath("answer").textValue();
        String creator = json.findPath("creator").textValue();
        JsonNode isDraft = json.findPath("skill");

        // Data Validation
        if (name == null ) { return badRequest(Json.toJson("Missing parameter [name]")); }
        if (question == null ) { return badRequest(Json.toJson("Missing parameter [question]")); }
        if (answer == null ) { return badRequest(Json.toJson("Missing parameter [answer]")); }
        if (creator == null ) { return badRequest(Json.toJson("Missing parameter [creator]")); }
        if (isDraft.isNull()) { return badRequest(Json.toJson("Missing parameter [isDraft]")); }

        // Create Flashcard
        new Flashcard(name, question, answer, creator, isDraft.booleanValue()).save();
        return ok(Json.toJson("Success: 1 Flashcard created."));

    }

    /**
     * Edit Flashcard (PUT)
     *
     * @return      validation errors or success message
     */
    @BodyParser.Of(BodyParser.Json.class)
    @Security.Authenticated(Secured.class)
    public Result editFlashcard(Long id) {

        // Parse
        JsonNode json = request().body().asJson();
        String pk = json.findPath("id").textValue();
        String name = json.findPath("name").textValue();
        String question = json.findPath("question").textValue();
        String answer = json.findPath("answer").textValue();
        String creator = json.findPath("creator").textValue();
        JsonNode isDraft = json.findPath("isDraft");

        // Data Validation and Deserialization
        boolean dataChanged = false;
        Flashcard flashcard = Flashcard.find.byId(id);
        if (flashcard == null) {
            return notFound(Json.toJson("Flashcard id not found."));
        }
        if (pk != null) {
            if (Long.valueOf(pk).longValue() != id) {
                return badRequest(Json.toJson("Contradictory Flashcard id's provided"));
            }
        }
        if (name != null) { flashcard.name = name; dataChanged = true; }
        if (question != null) { flashcard.question = question; dataChanged = true; }
        if (answer != null) { flashcard.answer = answer; dataChanged = true; }
        if (creator != null) {
            if (User.find.byId(creator.toLowerCase()) == null) {
                return notFound(Json.toJson("Creator does not exist."));
            }
        }
        if (!isDraft.isNull()) { flashcard.isDraft = isDraft.booleanValue(); dataChanged = true; }
        if(!dataChanged) {
            return ok(Json.toJson("Success: No data changed."));
        }

        // Update Flashcard
        flashcard.update();
        return ok(Json.toJson("Success: 1 Flashcard updated."));

    }

    /**
     * Delete Flashcard (DELETE)
     * TODO: Need to determine whether associated ratings should cascading delete
     *
     * @return      validation errors or success message
     */
    @Security.Authenticated(Secured.class)
    public Result deleteFlashcard(Long id) {
        Flashcard flashcard = Flashcard.find.byId(id);
        if (flashcard == null) {
            return notFound(Json.toJson("Flashcard id not found."));
        }
        Flashcard.find.ref(id).delete();
        return ok("Success: 1 Flashcard deleted.");
    }

    /**
     * Edit Report (PUT)
     * TODO: If missing parameters, they will be instantiated to NULL by default, which is bad
     *
     * @return      validation errors or success message
     */
    @BodyParser.Of(BodyParser.Json.class)
    @Security.Authenticated(Secured.class)
    public Result editReport(Long id) {

        // Parse
        JsonNode json = request().body().asJson();
        String pk = json.findPath("id").textValue();
        String status = json.findPath("status").textValue();


        // Data Validation and Deserialization
        boolean dataChanged = false;
        Report report = Report.find.byId(id);
        if (report == null) {
            return notFound(Json.toJson("Flashcard id not found."));
        }
        if (pk != null) {
            if (Long.valueOf(pk).longValue() != id) {
                return badRequest(Json.toJson("Contradictory Flashcard id's provided"));
            }
        }

        if (status != null) { report.status = status; dataChanged = true; }

        if(!dataChanged) {
            return ok(Json.toJson("Success: No data changed."));
        }


        // Update Flashcard
        report.update();
        return ok(Json.toJson("Success: 1 Flashcard updated."));

    }

    /**
     * Add Rating (POST)
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result addRating() {
        // Parse
        JsonNode json = request().body().asJson();
        int type = json.findPath("type").intValue();
        String rater = json.findPath("rater").textValue();
        Long flashcard = json.findPath("flashcard").longValue();
        Double value = json.findPath("value").asDouble();

        // Data Validation
        if (rater == null ) { return badRequest(Json.toJson("Missing parameter [rater]")); }
        if (flashcard == 0) { return badRequest(Json.toJson("Missing parameter [flashcard]")); }
        if (value == 0) { return badRequest(Json.toJson("Missing parameter [value]")); }
        if (User.find.byId(rater) == null) {
            return notFound("Username not found.");
        }
        if (Flashcard.find.byId(flashcard) == null) {
            return notFound("Flashcard id not found.");
        }
        if (!(type == 1 || type == 2)) {
            return badRequest("Invalid rating type.");
        }

        // Create Flashcard
        new Rating(type, rater, flashcard, value).save();
        return ok(Json.toJson("Success: 1 Flashcard created."));
    }
    
    /**
     * Save Flashcard (POST)
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result saveFlashcard(Long id) {
        JsonNode json = request().body().asJson();
        String username = json.findPath("requestor").textValue().toLowerCase();
        User user = User.find.byId(username);
        if (user == null) {
            return notFound("User id not found.");
        }
        user.collection = user.collection + id + " ";
        user.update();
        return ok(Json.toJson(user));
    }

    /**
     * Report Flashcard (POST)
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result reportFlashcard(Long id) {
        JsonNode json = request().body().asJson();

        String username = json.findPath("creator").textValue().toLowerCase();
        long flashcardId = id;
        String status = "new";
        String issue = json.findPath("issue").textValue();

        new Report(issue, status, username, flashcardId).save();

        User user = User.find.byId(username);
        Flashcard flashcard = Flashcard.find.byId(id);

        return ok(Json.toJson(user));
    }

    /**
     * Get flashcards for the play game (GET)
     *
     * I'm sorry for the messy code, but the majority of the messiness owes to the fact that I am trying to generate
     * a random list of UNIQUE flashcards without duplicates. In other words, I am tracking which flashcards I have
     * added to the result list, and ensuring that the entire process doesn't loop too many times.
     *
     * Note that this method will return some number of flashcards LESS THAN the specified limit (or total number of
     * flashcards in the database).
     *
     * @param difficulty    The skill level of the user to use as a seed
     * @param limit         The maximum number of flashcards to return
     * @return              A random list of unique flashcards (no duplicates) with flashcards more suitable for
     *                      the user at the beginning of the list.
     */
    public Result playFlashcards(double difficulty, int limit) {
        List<Flashcard> result = new ArrayList<Flashcard>();
        Map<Long, Boolean> added = new HashMap<Long, Boolean>();
        int maxSize = Flashcard.find.all().size();
        if (limit < maxSize) {
            maxSize = limit;
        }
        int counter = 0;
        int MAX_ITERATIONS = 10;
        while (maxSize > 0) {
            Flashcard flashcard = Flashcard.drawFlashcard(difficulty);
            if (added.get(flashcard.id) == null) {
                result.add(flashcard);
                added.put(flashcard.id, true);
                maxSize--;
                counter = 0;
            }
            if(counter > MAX_ITERATIONS) {
                maxSize--;
                counter = 0;
            }
            counter++;
        }
        return ok(Json.toJson(result));
    }

}
