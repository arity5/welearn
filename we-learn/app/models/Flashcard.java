package models;

import java.util.*;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.*;
import play.data.validation.*;
import play.db.ebean.*;
import com.avaje.ebean.Ebean;

/**
 * Project WeLearn - Flashcard Applicataion
 * COMSW4156 Advanced Software Engineering
 * Flashcard.java
 * Purpose: Flashcard Model class file
 *
 * @author  Arity5
 * @version 0.2
 */
@Entity
public class Flashcard extends Model {

    /**
     * Attributes
     */
    @Id
    public long id;
    @Constraints.Required
    public String name;
    @Constraints.Required
    public String question;
    @Constraints.Required
    public String answer;
    @ManyToOne
    public User creator;
    public Date timestamp;
    public boolean isDraft;
    
    //public static Finder<Integer, Flashcard> search = new Model.Finder<>(Integer.class, Flashcard.class);

    /**
     * This method attaches a Finder to Flashcard class; which is essentially a Flashcard Object Manager.
     * The finder basically allows us to formulate SQL queries on the User class.
     */
    public static Model.Finder<Long,Flashcard> find = new Model.Finder(Long.class, Flashcard.class);
    public static Model.Finder<String, Flashcard> search = new Model.Finder<>(String.class, Flashcard.class);
    
    /**
     * Constructor for the flashcard model
     *
     * @param name          the title of the flashcard
     * @param question      the question body of the flashcard
     * @param answer        the answer to the question
     * @param creator       the username of the User who created the flashcard
     * @param isDraft       signifies whether this flashcard is only visible to the creator
     */
    public Flashcard(String name, String question, String answer, String creator, boolean isDraft) {
        this.name = name;
        this.question = question;
        this.answer = answer;
        this.creator = User.find.byId(creator.toLowerCase());
        this.isDraft = isDraft;
        this.timestamp = new Date();
    }
    
    /**
     * Search
     * 
     */
    public static List<Flashcard> searchByTitle(String title){
        return search.where().ilike("name", "%"+title+"%").findList();
    }

    @JsonCreator
    /**
     * The JSON constructor for the User model. This is called when Json.fromJson() is evoked.
     *
     * @param name          the title of the flashcard
     * @param question      the question body of the flashcard
     * @param answer        the answer to the question
     * @param creator       the username of the User who created the flashcard
     * @param isDraft       signifies whether this flashcard is only visible to the creator
     */
    public static Flashcard create(@JsonProperty("name") String title,
                                   @JsonProperty("question") String q,
                                   @JsonProperty("answer") String a,
                                   @JsonProperty("creator") String user,
                                   @JsonProperty("isDraft") boolean d) {
        return new Flashcard(title, q, a, user, d);
    }

    /**
     * Returns the current difficulty rating for this flashcard
     *
     * @return              the difficulty rating computed for this flashcard
     */
    public double getDifficultyRating() {
        List<Rating> ratings = Rating.find.where()
                .eq("flashcard", this)
                .eq("type", Rating.DIFFICULTY)
                .findList();
        double sum = 0;
        int count = 0;
        for (Rating rating : ratings) {
            sum += rating.value;
            count++;
        }
        if (count == 0) {
            return 1; //Easiest possible difficulty
        }
        return sum / count;
    }

    /**
     * Returns the current quality rating for this flashcard
     *
     * @return              the quality rating computed for this flashcard
     */
    public double getQualityRating() {
        List<Rating> ratings = Rating.find.where()
                .eq("flashcard", this)
                .eq("type", Rating.QUALITY)
                .findList();
        double sum = 0;
        int count = 0;
        for (Rating rating : ratings) {
            sum += rating.value;
            count++;
        }
        if (count == 0) {
            return 3; //Average quality
        }
        return sum / count;
    }

    @JsonGetter
    /**
     * Override getter method for user field
     *
     * @return              show only username, not the entire user object
     */
    public String getCreator() {
        return this.creator.username;
    }

    /**
     * This method is a Flaschard Manager method that finds all flashcards that belong to a user.
     */
    public static List<Flashcard> findInvolving(String user) {
        return find.where().eq("creator.username", user).findList();
    }

    /**
     * This method is a Flaschard Manager method that randomly draws a flashcard with a loss function
     * around the difficulty parameter.
     *
     * I'm sorry this code is so ugly, but basically the concept of it is that we:
     *   1) draw a random card.
     *   2) Calculate the absolute value of the difference between the card's difficulty and the user's skill
     *   3) If the delta difference is 0, we keep the card. If the delta difference is greater than 0, there is a
     *      random chance we will throw away the card. The higher the delta, the greater the chance we will throw away
     *      the card and need to draw another one. Continue to draw cards until we keep one.
     */
    public static Flashcard drawFlashcard(double difficulty) {
        double CUTOFF_VALUE = 0.8;
        Random rnd = new Random();
        while(true) {
            Flashcard flashcard = getRandomFlashcard();
            if (Math.abs(flashcard.getDifficultyRating() - difficulty) * rnd.nextDouble() < CUTOFF_VALUE) {
                return flashcard;
            }
        }
    }

    /**
     * Get a random flashcard.
     *
     * @return      Flashcard, randomly selected.
     */
    public static Flashcard getRandomFlashcard() {
        Random random = new Random();
        int number = random.nextInt(Flashcard.find.all().size());
        String q = "find Flashcard where id=:id";
        Flashcard flashcard = Flashcard.find
                .setFirstRow(number)
                .setMaxRows(1)
                .findUnique();
        return flashcard;
    }

}
