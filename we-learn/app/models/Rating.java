package models;

import javax.persistence.*;
import play.db.ebean.*;
import java.util.*;

/**
 * Project WeLearn - Flashcard Applicataion
 * COMSW4156 Advanced Software Engineering
 * Rating.java
 * Purpose: Rating Model class file
 *
 * @author  Arity5
 * @version 0.2
 */
@Entity
public class Rating extends Model {

    /**
     * Constants
     */
    public static final int ERROR = 0;
    public static final int DIFFICULTY = 1;
    public static final int QUALITY = 2;

    /**
     * Attributes
     */
    @Id
    public long id;
    public int type;
    @ManyToOne
    public User rater;
    @ManyToOne
    public Flashcard flashcard;
    public double value;
    public Date timestamp;
    public boolean ignore;      //ignore is set to true if User resets their skill level

    /**
     * Constructor
     *
     * @param type          either 'DIFFICULTY' or 'QUALITY'; describes the rating type
     * @param user          string username of a user that exists in the db
     * @param flashcard     long id of a flashcard that exists in the db
     * @param value         value of the rating (typically 1-5)
     */
    public Rating(int type, String user, long flashcard, double value) {
        this.type = type;
        this.rater = User.find.byId(user.toLowerCase());
        this.flashcard = Flashcard.find.byId(flashcard);
        this.value = value;
        this.timestamp = new Date();
        this.ignore = false;
    }

    /**
     * This method attaches a Finder to Rating class; which is essentially a Rating Object Manager.
     * The finder basically allows us to formulate SQL queries on the User class.
     */
    public static Finder<Long,Rating> find = new Finder<Long,Rating>(Long.class, Rating.class);

}
