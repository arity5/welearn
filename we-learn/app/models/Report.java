package models;

import java.util.*;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.*;
import play.data.validation.*;
import play.db.ebean.*;
import com.avaje.ebean.Ebean;

/**
 * Project WeLearn - Flashcard Applicataion
 * COMSW4156 Advanced Software Engineering
 * Flashcard.java
 * Purpose: Report Model class file
 *
 * @author  Arity5
 * @version 0.2
 */
@Entity
public class Report extends Model {

    /**
     * Attributes
     */
    @Id
    public long id;
    @Constraints.Required
    public String issue;
    @Constraints.Required
    public String status;
    @Constraints.Required
    public User creator;
    @Constraints.Required
    public Flashcard flashcard;
    @Constraints.Required
    public Date timestamp;
    @Constraints.Required
    public String username;
    @Constraints.Required
    public String flashcardQuestion;
    @Constraints.Required
    public String flashcardAnswer;
    @Constraints.Required
    public String flashcardName;
    @Constraints.Required
    public long flashcardId;


    /**
     * Constructor for the report model
     *
     * @param issue         the issue of the reported flashcard
     * @param status        the status of the report
     * @param flashCardId   the id of the reported flashcard
     * @param creator       the username of the User who created the report
     */
    public Report(String issue, String status, String creator, long flashCardId) {
        this.issue = issue;
        this.status = status;
        this.creator = User.find.byId(creator.toLowerCase());
        this.flashcard = Flashcard.find.byId(flashCardId);
        this.timestamp = new Date();
        this.username = creator;
        this.flashcardQuestion = this.flashcard.question;
        this.flashcardAnswer = this.flashcard.answer;
        this.flashcardName = this.flashcard.name;
        this.flashcardId = this.flashcard.id;
    }

    @JsonCreator
    /**
     * The JSON constructor for the Report model. This is called when Json.fromJson() is evoked.
     *
     * @param issue         the issue of the reported flashcard
     * @param status        the status of the report
     * @param flashCardId   the id of the reported flashcard
     * @param creator       the username of the User who created the report
     */
    public static Report create(@JsonProperty("issue") String issue,
                                @JsonProperty("status") String status,
                                @JsonProperty("creator") String creator,
                                @JsonProperty("flashCardId") long flashCardId) {
        return new Report(issue, status, creator, flashCardId);
    }

    /**
     * This method attaches a Finder to Report class; which is essentially a Flashcard Object Manager.
     * The finder basically allows us to formulate SQL queries on the User class.
     */
    public static Model.Finder<Long,Report> find = new Model.Finder(Long.class, Report.class);

}
