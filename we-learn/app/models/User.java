package models;

import javax.persistence.*;
import play.mvc.*;
import play.data.validation.*;
import com.avaje.ebean.*;
import com.fasterxml.jackson.databind.annotation.*;
import com.fasterxml.jackson.annotation.*;
import java.util.*;

/**
 * Project WeLearn - Flashcard Applicataion
 * COMSW4156 Advanced Software Engineering
 * User.java
 * Purpose: User Model class file
 *
 * @author  Arity5
 * @version 0.2
 */
@Entity
public class User extends Model {

    /**
     * Attributes
     */
    @Id
    public String username;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;
    @JsonIgnore
    public double initialSkill; //Do not set this value directly. Please use setSkill() instead.
    public String collection;

    /**
     * The constructor for the User model.
     *
     * @param username      the user's username, must be all lowercase and ascii
     * @param email         the user's email address,  must be all lowercase and ascii
     * @param password      the user's password, must be ascii
     */
    public User(String username, String email, String password) {
    	this.email = email.toLowerCase();
	    this.username = username.toLowerCase();
	    this.password = password;
        this.initialSkill = 1;
	    this.collection = "";
    }

    @JsonGetter
    /**
     * Override getter method for password field
     *
     * @return              do not show password text; replace with stars
     */
    public String getPassword() {
        return this.password.replaceAll(".", "*");
    }

    /**
     * Computes the skill level of the User. This is computed by taking the average of all of a user's DIFFICULTY
     * ratings.
     *
     * @return          the computed skill of the user, typically a value between 1-5.
     */
    public double getSkill() {
        List<Rating> ratings = Rating.find.where()
                .eq("rater", this)
                .eq("type", Rating.DIFFICULTY)
                .eq("ignore", false)
                .findList();
        double sum = this.initialSkill;
        int count = 1;
        for (Rating rating : ratings) {
            sum += rating.value;
            count++;
        }
        return sum / count;
    }

    /**
     * Sets the skill level of the User. By forcing a reset of the User Skill level, this causes all old DIFFICULTY
     * ratings associated with the User to be "ignored" for all future getSkill() purposes.
     * TODO: Currently, skill is a required JSON field, which means the Skill will be reset with every request (which is bad)
     *
     * @param value     the value to set the User's skill level to
     */
    public void setSkill(double value) {
        // Probably more efficient to use raw SQL but it feels like bad practice to hardcode a SQL statement.
        // It is probably inefficient to use a for loop to sequentially update each record.
        List<Rating> ratings = Rating.find.where()
                .eq("rater", this)
                .eq("type", Rating.DIFFICULTY)
                .eq("ignore", false)
                .findList();
        for (Rating rating : ratings) {
            rating.ignore = true;
            rating.update();
        }
        this.initialSkill = value;
    }

    /**
     * Check whether the given username and password exist in the database.
     *
     * @param username      the user's username
     * @param password      the user's password
     * @return              the matched user.
     *                      if no match or non-unique match, returns null
     */
    public static User authenticate(String username, String password) {
	    return find.where().eq("username", username.toLowerCase()).eq("password", password).findUnique();
    }

    /**
     * This method attaches a Finder to User class; which is essentially a User Object Manager.
     * The finder basically allows us to formulate SQL queries on the User class.
     */
    public static Finder<String,User> find = new Finder<String,User>(String.class, User.class);

}