package views.forms;

import models.User;

/**
 * Defines a login form for lazy templating.
 *
 * Server-side form validation goes here. Technically, in my opinion, this really should be on the controller
 * side of things, but I couldn't figure out how to get it to work according to the 2.1.1 authentication tutorial.
 * I saw some code online that did this, so its organized this way.
 */
public class LoginForm {

    public String username;
    public String password;

    /**
     * This is the main validation function for logging in. It is automatically called when .hasErrors() is
     * evoked in the Application controller. Don't ask me why this is -- the framework is designed this way.
     *
     * You can probably see from this that there are multiple ways to do accomplish this. You could write the entire
     * validation function inside the controller and ignore this; you could write it entirely in the model;
     * or you can do a mixture of all of them -- which is how it presently is.
     */
    public String validate() {
        if (User.authenticate(username, password) == null) {
            return "Invalid user or password";
        }
        return null;
    }

}