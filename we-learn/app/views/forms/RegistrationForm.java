package views.forms;

import java.util.*;
import play.data.validation.*;
import models.*;

/**
 * Defines a registration form for lazy templating.
 *
 * See LoginForm.java for notes.
 */
public class RegistrationForm {

    @Constraints.Required
    public String username;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String password;

    /**
     * This is the main validation function for registration. It is automatically called when .hasErrors() is
     * evoked in the Application controller. Don't ask me why this is -- the framework is designed this way.
     */
    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();

        // Test if user already exists
        User newUser = User.find.byId(username);
        if (newUser != null) {
            errors.add(new ValidationError("username", "This username is already registered."));
        }

        // Test if email address is valid
        if (!email.contains("@") || !email.contains(".")) {
            errors.add(new ValidationError("email", "This is an invalid email address."));
        }
        return errors.isEmpty() ? null : errors;
    }

}