# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table flashcard (
  id                        bigint not null,
  name                      varchar(255),
  question                  varchar(255),
  answer                    varchar(255),
  creator_username          varchar(255),
  timestamp                 timestamp,
  is_draft                  boolean,
  constraint pk_flashcard primary key (id))
;

create table rating (
  id                        bigint not null,
  type                      integer,
  rater_username            varchar(255),
  flashcard_id              bigint,
  value                     double,
  timestamp                 timestamp,
  ignore                    boolean,
  constraint pk_rating primary key (id))
;

create table report (
  id                        bigint not null,
  issue                     varchar(255),
  status                    varchar(255),
  timestamp                 timestamp,
  username                  varchar(255),
  flashcard_question        varchar(255),
  flashcard_answer          varchar(255),
  flashcard_name            varchar(255),
  flashcard_id              bigint,
  constraint pk_report primary key (id))
;

create table user (
  username                  varchar(255) not null,
  email                     varchar(255),
  password                  varchar(255),
  initial_skill             double,
  collection                varchar(255),
  constraint pk_user primary key (username))
;

create sequence flashcard_seq;

create sequence rating_seq;

create sequence report_seq;

create sequence user_seq;

alter table flashcard add constraint fk_flashcard_creator_1 foreign key (creator_username) references user (username) on delete restrict on update restrict;
create index ix_flashcard_creator_1 on flashcard (creator_username);
alter table rating add constraint fk_rating_rater_2 foreign key (rater_username) references user (username) on delete restrict on update restrict;
create index ix_rating_rater_2 on rating (rater_username);
alter table rating add constraint fk_rating_flashcard_3 foreign key (flashcard_id) references flashcard (id) on delete restrict on update restrict;
create index ix_rating_flashcard_3 on rating (flashcard_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists flashcard;

drop table if exists rating;

drop table if exists report;

drop table if exists user;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists flashcard_seq;

drop sequence if exists rating_seq;

drop sequence if exists report_seq;

drop sequence if exists user_seq;

