# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

GET     /                           controllers.Application.index()

GET     /user/register              controllers.Application.registrationPage()
POST    /user/register              controllers.Application.register()

GET     /user/login                 controllers.Application.loginPage()
POST    /user/login                 controllers.Application.login()
GET     /user/logout                controllers.Application.logout()

GET     /user/                      controllers.Application.userHome()
GET     /user/get                   controllers.Application.getUser()
GET	    /user/settings		        controllers.Application.settings()
GET	    /user/search		        controllers.Application.search()

GET     /moderation/                controllers.Application.moderationHome()

GET     /api/user/all               controllers.Api.getUsers()
GET     /api/user/:id               controllers.Api.getUser(id)
POST    /api/user/add               controllers.Api.addUser()
PUT     /api/user/:id/edit          controllers.Api.editUser(id)
DELETE  /api/user/:id/delete        controllers.Api.deleteUser(id)

GET     /api/report/all             controllers.Api.getReports()
GET     /api/report/new             controllers.Api.getNewReports()
PUT     /api/report/:id/edit        controllers.Api.editReport(id: Long)

GET     /api/flashcard/all          controllers.Api.getFlashcards()
GET     /api/flashcard/search/:name controllers.Api.getFlashcardsByName(name)
GET     /api/flashcard/all/:user    controllers.Api.getFlashcardsByUser(user, limit: Integer ?= 10, offset: Integer ?= 0)
GET     /api/flashcard/play         controllers.Api.playFlashcards(difficulty: Double ?= 0, limit: Integer ?= 100)
GET     /api/flashcard/:id          controllers.Api.getFlashcard(id: Long)
POST    /api/flashcard/add          controllers.Api.addFlashcard()
POST    /api/flashcard/:id/report   controllers.Api.reportFlashcard(id: Long)
PUT     /api/flashcard/:id/edit     controllers.Api.editFlashcard(id: Long)
POST    /api/flashcard/:id/save     controllers.Api.saveFlashcard(id: Long)
DELETE  /api/flashcard/:id/delete   controllers.Api.deleteFlashcard(id: Long)

POST    /api/rating/add             controllers.Api.addRating()

GET     /play                       controllers.Application.play()

GET     /test                       controllers.Application.test()

# Javascript routing
GET    /assets/javascripts/routes   controllers.Application.javascriptRoutes()

# Map static resources from the /public folder to the /assets URL path
GET     /assets/*file               controllers.Assets.versioned(path="/public", file: Asset)

